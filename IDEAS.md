# IDEAS

For the 420 Pay Center we’re leveraging a decentralized JWT passing scheme 
that’s incredibly Smalltalk-like.


1. On-demand Cell Network at $2/mo: High Availability Low-cost Open source OpenBTS PBX with Asterisk 
2. AML/KYC via API with Blockscore
3. Torrens Easements Modeling
4. Reactive Analytics and Frequency Information UI Expressions for Pages and Maps
5. Social Tipping
6. Credit Asset Trading
7. EcoPoints/Carbon Credits
8. Rewards as Assets: Dealing with Point Stripping through Nearest Neighbor Proof
9. Change Management: Engagement, Adoption, Readiness, Sustainability
10. BitReserve, Blockscore, etc., etc.

