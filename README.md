# 420 Pay Center Project

Access http://oktoken.herokuapp.com.

## Setup

Run `grunt setup` to install dependencies for the front end.

## Build & development

Run `grunt` for building and `grunt devel` for development

## Serving Locally

Run the local server with `node server/www.js`.

## Style Compilation

Move into the `app/styles/` folder and run `sass --watch sass:css`

## Testing

Running `grunt test` will run the unit tests with karma.

## Management

### “Building” content from static site copy

Content Authors (CA) may use http://prose.io to update the `contents` directory.

This project uses [Punch](https://github.com/laktek/punch/wiki) to convert 
Moustache templates into static site pages that essentially map public URLs 
to files created in the repository. Github, effectively, and logical naming of,
files becomes the CMS. This should happen during the Build phase (Grunt).
