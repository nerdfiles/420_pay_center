'use strict';

describe('Controller: MainCtrl', function () {

  beforeEach(module('blockauction'));

  var MainCtrl,
    scope;

  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of scraped items to the scope', function () {
    expect(scope.items.length).toBeTruthy();
  });
});
