'use strict';

define([
  "angularAMD",
  "leaflet",
  "osm",
  "routes",
  "jquery",
  "jquery.unveil",
  "angular-route",
  "angular-animate",
  "angular-aria",
  "angular-messages",
  "angular-cookies",
  "angular-resource",
  "angular-sanitize",
  "angular-storage",
  "angularfire",
  "angular-material",
  "directives/ngShowAuth",
  "directives/ngHideAuth"
  //"angular-waypoints"
], function (angularAMD, leaflet, OSMBuildings, routes, $) {

  //window.OSMBuildings = OSMBuildings;

  /**
   * @ngdoc overview
   * @name fourTwentyPayCenter
   * @description
   * # 420paycenter Web application.
   *
   * Main module of the application.
   */
  var app = angular.module("fourTwentyPayCenter", [
    'firebase',
    "ngRoute",
    "ngAnimate",
    "ngCookies",
    "ngResource",
    "ngSanitize",
    'ngAria',
    'ngMessages',
    'leaflet-directive',
    'ngStorage',
    'ngMaterial'
    //'zumba.angular-waypoints'
  ]);

  app
  .constant('FBURL', 'https://fourTwentyPayCenter.firebaseio.com')
  .constant('SIMPLE_LOGIN_PROVIDERS', ['password'])
  .constant('loginRedirectPath', '/login')

  .factory('Ref', [
    '$window',
    'FBURL',
    function($window, FBURL) {
      return new $window.Firebase(FBURL);
    }
  ])

  .factory('Auth', [
    '$firebaseAuth',
    'Ref',
    function($firebaseAuth, Ref) {
      return $firebaseAuth(Ref);
    }
  ])

  .provider('UserSession', [
    function () {
      this.$get = ['Auth', function (Auth) {
        return Auth.$requireAuth();
      }];
    }
  ])

  .provider('User', [
    function () {
      this.$get = [
        'UserSessionProvider',
        function (UserSessionProvider) {
          return UserSessionProvider.$get();
        }
      ];
    }
  ])

  .config(['$routeProvider', 'SECURED_ROUTES', 'UserProvider', function ($routeProvider, SECURED_ROUTES, User) {

    $routeProvider.whenAuthenticated = function (path, route) {
      route.resolve = route.resolve || {};
      //User.$get();
      $routeProvider.when(path, route);
      SECURED_ROUTES[path] = true;
      return $routeProvider;
    };
  }])

  .config([
    '$routeProvider',
    '$locationProvider',
    routes
  ])

  .run([
    '$rootScope',
    '$routeParams',
    '$location',
    'Auth',
    'SECURED_ROUTES',
    'loginRedirectPath',
    '$anchorScroll',
    function($rootScope, $routeParams, $location, Auth, SECURED_ROUTES, loginRedirectPath, $anchorScroll) {
      $rootScope.showMenu = true;
      $rootScope.toggleMenu = function () {
        $rootScope.showMenu = !$rootScope.showMenu;
      };

      $rootScope.scrollTo = function (id) {
        if ($location.$$path !== '/') {
          $location.path(id);
        }
        var old = $location.hash();
        $location.hash(id);
        $anchorScroll();
        $location.hash(old);
      };

      Auth.$onAuth(check);
      $rootScope.$on('$routeChangeError', function(e, next, prev, err) {
        if( err === 'AUTH_REQUIRED' ) {
          $location.path(loginRedirectPath);
        }
      });

      function check (user) {
        if ( !user && authRequired($location.path()) ) {
          $location.path(loginRedirectPath);
        }
      }

      function authRequired (path) {
        return SECURED_ROUTES.hasOwnProperty(path);
      }
    }
  ])

  .constant('SECURED_ROUTES', {
    '/page/account': true
  })
  .directive('unveil', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        $(element).unveil(0, function() {
          $(element).load(function() {
            this.style.opacity = 1;
          });
        });
      }
    };
  });

  return angularAMD.bootstrap(app);

});
