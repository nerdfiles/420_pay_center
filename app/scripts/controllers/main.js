'use strict';

/**
 * @ngdoc function
 * @name blockauction.controller:MainController
 * @description
 * # MainController
 * Main Controller — probably for a Splash screen that isn't "Home"...
 */
define([
  "interface"
], function (__interface__) {
  var MainController = function ($scope, $rootScope) {
    $rootScope.pageName = {
      'site--dashboard': true
    };
  };

  return [
    "$scope",
    "$rootScope",
    MainCtrl
  ];
});
