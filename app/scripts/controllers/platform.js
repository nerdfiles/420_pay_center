define([
  "interface",
  "angular",
  "directives/ngShowAuth",
  "directives/ngHideAuth",
  "filters/reverse"
], function (__interface__, angular) {
  function PlatformController ($scope, $rootScope, $http, $q, $anchorScroll, $location, $routeParams, Ref, $firebaseArray, $timeout) {
    $rootScope.pageName = {
      'site--platform': true
    };
    console.log($scope);

  }
  return [
    '$scope',
    '$rootScope',
    '$http',
    '$q',
    '$anchorScroll',
    '$location',
    '$routeParams',
    'Ref',
    '$firebaseArray',
    '$timeout',
    PlatformController
  ];
});
