define([
  "interface",
  "angular",
  "directives/ngShowAuth",
  "directives/ngHideAuth",
  "filters/reverse"
], function (__interface__, angular) {
  function KioskController ($scope, $rootScope, $http, $q, $anchorScroll, $location, $routeParams, Ref, $firebaseArray, $timeout) {
    $rootScope.pageName = {
      'site--kiosk': true
    };

  }
  return [
    '$scope',
    '$rootScope',
    '$http',
    '$q',
    '$anchorScroll',
    '$location',
    '$routeParams',
    'Ref',
    '$firebaseArray',
    '$timeout',
    KioskController
  ];
});

