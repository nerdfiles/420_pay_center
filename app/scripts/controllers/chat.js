'use strict';
/**
 * @ngdoc function
 * @name blockauction.controller:ChatController
 * @description
 * # ChatController
 * Conversational UI for with Bidders with clerk agents backed by NLP.
 */
define([
  "interface"
], function (__interface__){

  var ChatController = function ($scope, $rootScope, Ref, $firebaseArray, $timeout) {
    $rootScope.pageName = {
      'site--chat': true
    };
    $scope.messages = $firebaseArray(Ref.child('messages').limitToLast(10));
    $scope.messages.$loaded().catch(alert);
    $scope.addMessage = function(newMessage) {
      if( newMessage ) {
        $scope.messages.$add({text: newMessage})
          .catch(alert);
      }
    };

    function alert(msg) {
      $scope.err = msg;
      $timeout(function() {
        $scope.err = null;
      }, 5000);
    }
  };

  return [
    "$scope",
    "$rootScope",
    "Ref",
    "$firebaseArray",
    "$timeout",
    ChatController
  ];
});
