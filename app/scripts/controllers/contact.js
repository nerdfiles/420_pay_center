define([
  "interface",
  "angular",
  "directives/ngShowAuth",
  "directives/ngHideAuth",
  "filters/reverse"
], function (__interface__, angular) {
  function ContactController ($scope, $rootScope, $http, $q, $anchorScroll, $location, $routeParams, Ref, $firebaseArray, $timeout) {
    $rootScope.pageName = {
      'site--contact': true
    };
    console.log($scope);

  }
  return [
    '$scope',
    '$rootScope',
    '$http',
    '$q',
    '$anchorScroll',
    '$location',
    '$routeParams',
    'Ref',
    '$firebaseArray',
    '$timeout',
    ContactController
  ];
});


