define(['angularAMD'], function (angularAMD) {

    var routeStateMap = function ($routeProvider, $locationProvider) {

      $locationProvider.html5Mode(true);
      //$locationProvider.hashPrefix(!);

      $routeProvider

        .when("/:section?", angularAMD.route({
          templateUrl   : 'views/home.html',
          controllerUrl : 'controller/home_ctrl'
        }))

        .when("/page/kiosk", angularAMD.route({
          templateUrl   : 'views/kiosk.html',
          controllerUrl : 'controller/kiosk_ctrl'
        }))

        .when("/page/platform", angularAMD.route({
          templateUrl   : 'views/platform.html',
          controllerUrl : 'controller/platform_ctrl'
        }))

        .when("/page/contact", angularAMD.route({
          templateUrl   : 'views/contact.html',
          controllerUrl : 'controller/contact_ctrl'
        }))

        .when('/page/dashboard', angularAMD.route({
          templateUrl   : 'views/main.html',
          controllerUrl : 'controller/main_ctrl'
        }))

        .when('/page/chat', angularAMD.route({
          templateUrl   : 'views/chat.html',
          controllerUrl : 'controller/chat_ctrl'
        }))

        .when('/page/login', angularAMD.route({
          templateUrl   : 'views/login.html',
          controllerUrl : 'controller/login_ctrl'
        }))

        .whenAuthenticated('/page/account', angularAMD.route({
          templateUrl   : 'views/account.html',
          controllerUrl : 'controller/account_ctrl'
        }))

        .otherwise({redirectTo: '/'});

    };

    return routeStateMap;
});
