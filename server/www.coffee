###
@fileOverview ./server/www.coffee
@description

# Web Application Server

    The Web is fundamentally a distributed hypermedia application.

## Strategies

\#\#\# Environments

\#\#\# Performance

We might consider integration of load balancers here, along with other 
REST-ful apps.

\#\#\# Apps as Modules

\#\#\# Units

\#\#\# E2E

\#\#\# Tasks and Builds

###

Promise = require('bluebird')
express = require('express')

app = '420_pay_center'

Promise.resolve(express())
  .then(require('../' + app + '/server'))
  .then(require('../' + app + '/interface'))
  .then(require('../' + app + '/middleware'))
  .then(require('../' + app + '/theme'))
  .then(require('../' + app + '/start'))
