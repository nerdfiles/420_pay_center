
/**
@fileOverview ./420_pay_center/theme.js
@description
Load payCenter theme.
 */

(function() {
  var express, path, themeRouteConfig;

  express = require('express');

  path = require('path');

  themeRouteConfig = function(__interface__) {

    /**
    Theme Route Config
    @module payCenter.app/theme
     */
    __interface__.app.use(express["static"](path.join(__dirname, '../app')));
    return __interface__;
  };

  module.exports = themeRouteConfig;

}).call(this);
