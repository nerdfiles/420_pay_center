
/**
@fileOverview ./blocknext/server.js
 */

(function() {
  var bodyParser, config, cookieParser, express, http, path, server;

  http = require('http');

  express = require('express');

  bodyParser = require('body-parser');

  cookieParser = require('cookie-parser');

  config = {
    port: process.env['PORT'] || 3007
  };

  path = require('path');

  server = function(app) {
    var connectExpressServer;
    app.set('port', config.port);
    app.set('views', path.join(__dirname, '../app'));
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.use(bodyParser.json());
    app.use(cookieParser());
    connectExpressServer = function() {

      /**
      Initialize Application Server
       */
      return server = http.createServer(app);
    };
    return {
      app: app,
      server: connectExpressServer()
    };
  };

  module.exports = server;

}).call(this);
