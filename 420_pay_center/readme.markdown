# 420 Pay Center

## Platitudes of Competition

Where if two users become entangled in a “bid-off” (three consecutive 
“included as last leader or seller”) this creates a respect matrix where increasing 
stakes between users shows respect and decreasing stakes shows disrespect 
for the matrix. Multiply the matrix by itself to create field of trust such 
that users respect matrixes are used to express a modality of the auction 
item that basically is a spectrum of “friendly” competition to “aggressive”.
